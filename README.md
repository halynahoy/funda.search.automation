# README #

### What is this repository for? ###

* Automated Funda For sale (default) page, that allows the user to search for properties directly from the home page. 

* Used:

1. Selenium WebDriver
2. [SpecFlow](http://specflow.org/) 
3. NUnit

### What was covered ###

* For sale (default) page

1. Check for default values
2. Search without entering any search criteria
3. Valid city/postcode/area entered as search criteria
4. Incorrect values are typed in address field
5. Combination of all filters
6. From number is bigger than To 
7. Autocomplete for text field
8. Link to previously searched results 


### What was not covered ###

* Custom price: valid/invalid (started but was taking to long so left it)
* Custom price: up/down and remove(X) button
* More precise check that after Search I see suggested properties with selected filters
* Localization (Dutch)


### What would I do if I have more time ###

* Everything that was not covered
* Mainly similar tests to cover Huur, Nieuwbouw, Recreatie and Europa (would create separate pages and extract shared steps to the separate class)