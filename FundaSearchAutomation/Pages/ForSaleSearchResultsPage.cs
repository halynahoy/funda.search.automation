﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace FundaSearchAutomation.Pages
{
    public class ForSaleSearchResultsPage
    {
        private static IWebDriver _driver;

        public IWebElement FundaLogo
        {
            get { return _driver.FindElement(By.ClassName("logo")); }
        }

        public IWebElement ResultsCountNumber
        {
            get { return _driver.FindElement(By.ClassName("search-no-results-header")); }
        }

        public string PageName
        {
            get
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
                return wait.Until((d) => d.Title);
            }
        }

        public ForSaleSearchResultsPage(IWebDriver webDriver)
        {
            _driver = webDriver;

        }

        public ForSaleMainPage PressFunda()
        {
            FundaLogo.Click();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            var mainPage = new ForSaleMainPage();
            return mainPage;
        }

        public string GetResultsCountNumber => ResultsCountNumber.Text;
    }
}
