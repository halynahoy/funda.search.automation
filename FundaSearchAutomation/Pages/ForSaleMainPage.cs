﻿using System;
using System.Threading;
using FundaSearchAutomation.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace FundaSearchAutomation
{
    public class ForSaleMainPage
    {
        private static IWebDriver _driver;

        public IWebElement TextField
        {
            get
            {
                return _driver.FindElement(By.Id("autocomplete-input"), 5);
            }
        }

        public SelectElement DistanceElement
        {
            get
            {
                return new SelectElement(_driver.FindElement(By.Id("Afstand"), 5));
            }
        }

        public SelectElement FromElement
        {
            get
            {
                return new SelectElement(_driver.FindElement(By.Id("range-filter-selector-select-filter_fundakoopprijsvan"), 5));
            }
        }

        public IWebElement FromInput
        {
            get { return _driver.FindElement(By.Name("filter_FundaKoopPrijsVan")); }
        }

        public SelectElement ToElement
        {
            get
            {
                return new SelectElement (_driver.FindElement(By.Id("range-filter-selector-select-filter_fundakoopprijstot"), 5));
            }
        }

        public IWebElement ToInput
        {
            get { return _driver.FindElement(By.Name("filter_FundaKoopPrijsTot")); }
        }
        
        public IWebElement SearchButton
        {
            get
            {
                return _driver.FindElement(By.ClassName("button-primary-alternative"), 5);
            }
        }

        public IWebElement ErrorMessage
        {
            get
            {
                return _driver.FindElement(By.ClassName("autocomplete-no-suggestion-message"), 5);
            }
        }
        
        public IWebElement ForSaleButton
        {
            get
            {
                return _driver.FindElement(By.LinkText("For sale"), 5);
            }
        }

        public IWebElement ForRentButton
        {
            get
            {
                return _driver.FindElement(By.LinkText("For rent"), 5);
            }
        }

        public IWebElement NewlyBuiltButton
        {
            get
            {
                return _driver.FindElement(By.LinkText("Newly built"), 5);
            }
        }

        public IWebElement RecreationButton
        {
            get
            {
                return _driver.FindElement(By.LinkText("Recreation"), 5);
            }
        }

        public IWebElement EuropeButton
        {
            get
            {
                return _driver.FindElement(By.LinkText("Europe"), 5);
            }
        }

        public IWebElement LinkLastSearched
        {
            get { return _driver.FindElement(By.ClassName("link-alternative")); }
        }



        public IWebElement AutocompleteElement
        {
            get { return _driver.FindElement(By.Id("autocomplete-list")); }
        }

        public static ForSaleMainPage NavigateTo(IWebDriver webDriver)
        {
            _driver = webDriver;
            _driver.Navigate().GoToUrl("http://www.funda.nl/en/");
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            var mainPage = new ForSaleMainPage();
            return mainPage;
        }
        
        public ForSaleSearchResultsPage PressSearch()
        {
            SearchButton.Click();
            Thread.Sleep(2000);
            var searchResultsPage = new ForSaleSearchResultsPage(_driver);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            return searchResultsPage;
        }
        
        public bool IsNavigationLinkActive(IWebElement navigationLink)
        {
            return navigationLink.GetParent().HasClass("is-active");
        }
        
        public bool IsAutocompleteListActive()
        {
            return AutocompleteElement.HasClass("is-open");
        }

        public string FillInLocation
        {
            set
            {
                TextField.SendKeys(value);
            }
        }

        public string FillInFrom
        {
            set
            {
                FromInput.SendKeys(value);
            }
        }

        public string FillInTo
        {
            set
            {
                ToInput.SendKeys(value);
            }
        }

        public string ErrorMessageText => ErrorMessage.Text;

        public string DistanceValue => this.DistanceElement.SelectedOption.Text;

        public string FromValue => this.FromElement.SelectedOption.Text;

        public string ToValue => this.ToElement.SelectedOption.Text;


        public string GetLinkLastSearched => LinkLastSearched.GetAttribute("href");
    }
}
