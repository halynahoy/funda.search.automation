﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace FundaSearchAutomation
{
    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }

        public static IWebElement GetParent(this IWebElement el)
        {
            return el.FindElement(By.XPath(".."));
        }

        public static bool HasClass(this IWebElement el, string className)
        {
            var classes = el.GetAttribute("class");

            if (!string.IsNullOrEmpty(classes))
            {
                return classes.Contains(className);
            }

            return false;
        }
    }
}
