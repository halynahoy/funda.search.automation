﻿using System;
using FundaSearchAutomation.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace FundaSearchAutomation.StepDefinitions
{
    [Binding]
    public class FundaMainSearchSteps
    {
        private static ForSaleMainPage _forSaleMainSearchPage;
        private static ForSaleSearchResultsPage _forSaleSearchResults;
        private IWebDriver _driver;

        [BeforeScenario()]
        public void Setup()
        {
            _driver = new ChromeDriver();
        }

        [AfterScenario()]
        public void TearDown()
        {
            _driver.Quit();
        }

        [Given(@"I navigated to Funda website")]
        public void GivenINavigatedToFundaWebsite()
        {
            _forSaleMainSearchPage = ForSaleMainPage.NavigateTo(_driver);
        }

        [Given(@"I am on the main page")]
        public void GivenIAmOnTheMainPage()
        {
            Assert.IsTrue(_forSaleMainSearchPage.SearchButton.Displayed);
        }

        [When(@"I press Search button")]
        public void WhenIPressSearchButton()
        {
            _forSaleSearchResults = _forSaleMainSearchPage.PressSearch();
        }

        [Then(@"I should be redirected to the Results page for all netherlands")]
        public void ThenIShouldBeRedirectedToTheResultsPageAllNetherlands()
        {
            Assert.AreEqual(_forSaleSearchResults.PageName, "Houses for sale in Nederland [funda]");
        }

        [Then(@"I should be redirected to the Results page")]
        public void ThenIShouldBeRedirectedToTheResultsPage()
        {
            Assert.IsTrue(_forSaleSearchResults.PageName.Contains("Houses for sale in"));
        }

        [Then(@"I should see all houses for sale in the Netherlands")]
        public void ThenIShouldSeeAllHousesForSaleInTheNetherlands()
        {
            string expectedlink = "http://www.funda.nl/en/koop/heel-nederland/";
            Assert.AreEqual(expectedlink, _driver.Url);
        }

        [When(@"I enter (.*) in a text field")]
        public void WhenIEnterInATextField(string inputText)
        {
            _forSaleMainSearchPage.FillInLocation = inputText;
        }

        [When(@"I should see (.*) in the results page")]
        public void WhenIShouldSeeInTheResultsPage(string expectedLink)
        {
            Assert.AreEqual(expectedLink, _driver.Url);
        }

        [Then(@"I should see a message telling me that location cannot be found")]
        public void ThenIShouldSeeAMessageTellingMeThatLocationCannotBeFound()
        {
            Assert.AreEqual("Oops! Unfortunately we can not find this location.", _forSaleMainSearchPage.ErrorMessageText);
        }

        [Then(@"For Sale option is selected by default")]
        public void ThenForSaleOptionIsSelectedByDefault()
        {
            Assert.IsTrue(_forSaleMainSearchPage.IsNavigationLinkActive(_forSaleMainSearchPage.ForSaleButton));
        }

        [Then(@"Distance is (.*)")]
        public void ThenDistanceIsKm(string defaultDistance)
        {

            Assert.AreEqual(defaultDistance, _forSaleMainSearchPage.DistanceValue);
        }

        [Then(@"From is (.*)")]
        public void ThenFromIs(string defaultFrom)
        {
            Assert.AreEqual(defaultFrom, _forSaleMainSearchPage.FromValue);
        }

        [Then(@"To is (.*)")]
        public void ThenToIsNoMaximum(string defaultTo)
        {
            Assert.AreEqual(defaultTo, _forSaleMainSearchPage.ToValue);
        }

        [When(@"I select (.*) distance from dropdown")]
        public void WhenISelectDistanceFromDropdown(string distance)
        {
            _forSaleMainSearchPage.DistanceElement.SelectByText(distance);
        }

        [When(@"I select (.*) in From field")]
        public void WhenISelectInFromField(string from)
        {
            _forSaleMainSearchPage.FromElement.SelectByText(from);
        }

        [When(@"I select (.*) in To field")]
        public void WhenISelectInToField(string to)
        {
            _forSaleMainSearchPage.ToElement.SelectByText(to);
        }

        //[When(@"I enter (.*) in From field")]
        //public void WhenIEnterInFromField(string fromCustom)
        //{
        //    mainSearchPage.FillInFrom = fromCustom;
        //}

        //[When(@"enter (.*) in To field")]
        //public void WhenEnterInToField(string toCustom)
        //{
        //    mainSearchPage.FillInTo = toCustom;
        //}

        [Then(@"(.*) placeholder displayed")]
        public void PlaceholderDisplayed(string placeholderText)
        {
            Assert.AreEqual(placeholderText, _forSaleMainSearchPage.TextField.GetAttribute("placeholder"));
        }

        [When(@"I press Back button")]
        public void WhenIPressBackButton()
        {
            _driver.Navigate().Back();
        }

        [Then(@"I should see For sale main page")]
        public void ThenIShouldSeeForSaleMainPage()
        {
            Assert.AreEqual("http://www.funda.nl/en/koop/", _driver.Url);
            Assert.IsTrue(_forSaleMainSearchPage.ForSaleButton.Selected);
        }

        [When(@"I press Funda button")]
        public void WhenIPressFundaButton()
        {
            _forSaleMainSearchPage = _forSaleSearchResults.PressFunda();
        }

        //[Then(@"I should see (.*) in From field")]
        //public void ThenIShouldSeeInFromField(string expectedFromValue)
        //{
        //    Assert.AreEqual(expectedFromValue, mainSearchPage.FromValue);
        //}

        //[Then(@"I should see (.*) in To field")]
        //public void ThenIShouldSeeInToField(string expectedToValue)
        //{
        //    Assert.AreEqual(expectedToValue, mainSearchPage.ToValue);

        //}

        [Then(@"(.*) field still should be empty")]
        public void ThenFieldStillShouldBeEmpty(string priceInput)
        {
            string inputValue;

            switch (priceInput)
            {
                case "From":
                    inputValue = _forSaleMainSearchPage.FromValue;
                    break;
                case "To":
                    inputValue = _forSaleMainSearchPage.ToValue;
                    break;
                default:
                    throw new ArgumentException("Invalid Input name");
            }

            Assert.AreEqual(string.Empty, inputValue);
        }

        [Then(@"I should see (.*) and red highlighting on To field")]
        public void ThenIShouldSeeResultsAndRedHighlightingOnToField(string expectedNumberOfResults)
        {
            Assert.AreEqual(expectedNumberOfResults, _forSaleSearchResults.GetResultsCountNumber);
        }

        [Then(@"I should see link (.*) to previous search")]
        public void ThenIShouldSeeLinkToToPreviousSearch(string linkWithCityName)
        {
            Assert.AreEqual(linkWithCityName, _forSaleMainSearchPage.GetLinkLastSearched);
        }

        [Then(@"I should see suggested values in autocomplete component")]
        public void ThenIShouldSeeSuggestedValuesInAutocompleteComponent()
        {
            Assert.IsTrue(_forSaleMainSearchPage.IsAutocompleteListActive());
        }
    }
}