﻿Feature: Funda main search
	As a end-user
	I want to be able to search using search controls on the main Funda page 

Background: 
    Given I navigated to Funda website
	And I am on the main page

Scenario: [For sale] Test default values 
	Then For Sale option is selected by default
	And City, address, etc placeholder displayed 
	And Distance is + 0 km
	And From is € 0 
	And To is No maximum

Scenario: [For sale] Search without entering any search criteria
	When I press Search button
	Then I should be redirected to the Results page
	And I should see all houses for sale in the Netherlands 


Scenario Outline: [For sale] Search when valid city/postcode/area entered as search criteria  
	When I enter <valid_value> in a text field
	And I press Search button
	And I should see <expected_value> in the results page

	Examples: 
	| valid_value               | expected_value                                               |
	| Amsterdam                 | http://www.funda.nl/en/koop/amsterdam/                       |
	| 1811                      | http://www.funda.nl/en/koop/alkmaar/1811/                    |
	| Olympiadelaan, Amstelveen | http://www.funda.nl/en/koop/amstelveen/straat-olympiadelaan/ |
 
Scenario Outline: [For sale] Cannot search when incorrect values are typed in address field 
	When I enter <invalid_value> in a text field
	And I press Search button
	Then I should see a message telling me that location cannot be found

	Examples: 
	| invalid_value       |
	| TextText18113333333 |
	| VeryLongTextVeryLongTextVeryLongTextVeryLongTextVeryLongTextVeryLongTextVeryLongText |

Scenario Outline: [For sale] Search with a combination of all filters
	When I enter <text_value> in a text field
	And I select <distance> distance from dropdown
	And I select <from> in From field
	And I select <to> in To field
	And I press Search button
	Then I should be redirected to the Results page
	#And I should see houses in <text_value> 
	#And With <distance> of distance 
	#And Price range is <from> to <to> 

	Examples: 
	| text_value | distance | from      | to         |
	| Utrecht    | + 15 km   | € 175,000 | € 300,000  |
	| Alkmaar    | + 5 km   | € 100,000 | € 200,000  |
	| 1811       | + 0 km   | € 0       | No maximum |

Scenario: [For sale] From number is bigger than To 
	When I select € 250,000 in From field
	And I select € 100,000 in To field
	And I press Search button
	Then I should be redirected to the Results page for all netherlands
	Then I should see 0 results and red highlighting on To field

#Scenario: [For sale] Search with custom price range and returning back to the main page
#	When I select Custom in From field
#	And I enter 12345 in From field
#	Then I should see 12345 in From field
#	When I select Custom in To field
#	And enter 9999999 in To field
#	Then I should see 9999999 in To field
#	When I press Search button
#	Then I should be redirected to the Results page
#	When I press Back button
#	Then I should see For sale main page

#Scenario: [For sale] Verify that only numbers are acceptable in range fields
#	When I select Custom in From field
#	And I enter ABC in From field
#	Then From field still should be empty
#	When I select Custom in To field
#	And enter @#$ in To field
#	Then To field still should be empty
#	When I press Search button
#	Then I should see all houses for sale in the Netherlands 

#bug: it is possible to type 'e' in the From/To field

Scenario: [For sale] Verify Autocomplete for text field
	When I enter Eindho in a text field
	Then I should see suggested values in autocomplete component

Scenario: [For sale] Verify link to previously searched results 
	When I enter Alkmaar in a text field
	And I press Search button
	And I should see http://www.funda.nl/en/koop/alkmaar/ in the results page
	When I press Funda button
	Then I should see link http://www.funda.nl/en/koop/alkmaar/ to previous search

#localization 
#bug: When instead of Funda button press Back the link will show "All Netherlads" 

######

#Scenario: [For rent] Search without entering any search criterias	
#	And I press For rent button
#	And I press Search button
#	Then I should be redirected to the Results page
#	And I should see all houses for sale in the Netherlands 
#
#
#Scenario Outline: [For rent] Search when valid city/postcode/area entered as search criteria  
#	When I enter <valid_value> in a text field
#	And I press Search button
#	And I should see <expected_value> in the results page
#
#	Examples: 
#	| valid_value               | expected_value                                               |
#	| Amsterdam                 | http://www.funda.nl/en/koop/amsterdam/                       |
#	| 1811                      | http://www.funda.nl/en/koop/alkmaar/1811/                            |
#	| Olympiadelaan, Amstelveen | http://www.funda.nl/en/koop/amstelveen/straat-olympiadelaan/ |
# 
#Scenario Outline: [For rent] Cannot search when incorrect values are typed in address field 
#	When I enter <invalid_value> in a text field
#	And I press Search button
#	Then I should see a message telling me that lacation cannot be found
#
#	Examples: 
#	| invalid_value		 |
#	| @#$$%&*			 |
#	| 18113333333        |
#	| Just a simple text |
#
#Scenario Outline: [For rent] Search with a combination of all filters
#	When I enter <text_value> in a text field
#	And I select <distance> from dropdown
#	And I select <from> in From field
#	And I select <to> in To field
#	And I press Search button
#	Then I should be redirected to the Results page
#	Then I should see houses in <text_value> within <distance> of distance range and <from> to <to> price range
#
#	Examples: 
#	| text_value | distance | from      | to        |
#	| Haarlem    | + 1 km   | € 175,000 | € 240,000 |
#	| Alkmaar    | + 5 km   | € 100,000 | € 200,000 |
#
#Scenario: [For rent] From number is bigger than To 
#	When I select € 240,000 in From field
#	And I select € 100,000 in To field
#	And I press Search button
#	Then I should be redirected to the Results page
#	Then I should see 0 results and red highlighting on To field
#
#######
#Scenario: [Newly built] Search without entering any search criterias
#	When I press Newly built button
#	And I press Search button
#	Then I should be redirected to the Newly built Results page 
#	And I should see all houses that are newly buit in the Netherlands 
#
#Scenario Outline: [Newly built] Search when valid city/postcode/area entered as search criteria  
#	When I press Newly built button
#	And I enter <valid_value> in a text field
#	And I press Search button
#	And I should see <expected_value> in the results page
#
#	Examples: 
#	| valid_value            | expected_value                                                 |
#	| Den Haag               | http://www.funda.nl/en/nieuwbouw/den-haag/					  |
#	| 1814                   | http://www.funda.nl/en/nieuwbouw/1814/                         |
#	| Luttik Oudorp, Alkmaar | http://www.funda.nl/en/nieuwbouw/alkmaar/straat-luttik-oudorp/ |
#
#Scenario Outline: [Newly built] Cannot search when incorrect values are typed in address field 
#	When I press Newly built button
#	And I enter <invalid_value> in a text field
#	And I press Search button
#	Then I should see a message telling me that lacation cannot be found
#
#	Examples: 
#	| invalid_value		 |
#	| @#$$%&*			 |
#	| 18113333333        |
#	| Just a simple text |
#
#Scenario: [Recreation] Search for Recreation 
#	When I press Recriation button
#	Then I should be redirected to the Recriation page
#
#Scenario: [Europe] Search for Europe
#	When I press Europe button
#	Then I should be redirected to the Europe page



#possible bug #1: Adress field is cleared when users returns back on main page (all the rest are not cleared)
#possible bug #2: User searched for custom price in To and From field and then returned back on main page To and From 
                  #have 'Custom' values (not specified values). 
				  #Workaround is to select some price in a dropdown and then select custom value again.